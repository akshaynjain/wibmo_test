package com.akshay.wibmo.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.RotateAnimation
import android.widget.FrameLayout
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.akshay.wibmo.R
import com.akshay.wibmo.model.RecyclerItem
import java.util.*
import kotlin.collections.ArrayList

class ExpandableAdapter(private val context:Context): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var itemList: MutableList<RecyclerItem> = Collections.emptyList()
    private var items:ArrayList<RecyclerItem> = ArrayList()
    companion object {
        const val HEADER = 0
        const val CONTENT = 1
        const val FOOTER = 2
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater: LayoutInflater = LayoutInflater.from(context)
        var holder: RecyclerView.ViewHolder? = null
        when(viewType){
            HEADER->holder=
                HeaderViewHolder(inflater.inflate(R.layout.item_header,parent,false))
            CONTENT->holder=
                ContentViewHolder(inflater.inflate(R.layout.item_content,parent,false))

        }

        return holder ?: throw IllegalStateException("Item type unspecified.")
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun getItemViewType(position: Int): Int {
        return itemList[position].type
    }

    fun setData(itemList: List<RecyclerItem>) {
        this.itemList = itemList.toMutableList()
        notifyDataSetChanged()
    }

    fun getData():ArrayList<RecyclerItem>{
        val hs = HashSet<RecyclerItem>()
        hs.addAll(items)
        items.clear()
        items.addAll(hs)
        return items
    }

    fun add(position: Int, item: RecyclerItem) {
        items.add(position, item)
//        notifyItemInserted(position)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
       if (holder.adapterPosition!=RecyclerView.NO_POSITION){
            val item = itemList[holder.adapterPosition]
           when(item.type){
               HEADER -> {
                   bindHeader(holder as HeaderViewHolder, item)
               }
               CONTENT -> {
                   bindContent(holder as ContentViewHolder, item,position)
               }
           }
       }
    }

    // ViewHolder area

    class HeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val container: ConstraintLayout = itemView.findViewById(R.id.header_container)
        val title: TextView = itemView.findViewById(R.id.header_title)
        val arrow: ImageView = itemView.findViewById(R.id.header_arrow)
    }

    class ContentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val backgroundContainer: FrameLayout = itemView.findViewById(R.id.content_background_container)
        val container: ConstraintLayout = itemView.findViewById(R.id.content_container)
        val buttonAdd:ImageButton=itemView.findViewById(R.id.imageViewAdd)
        val buttonRemove:ImageButton=itemView.findViewById(R.id.imageViewRemove)
        val textViewCount:TextView=itemView.findViewById(R.id.textViewCount)
        val textViewPrice:TextView=itemView.findViewById(R.id.textViewPrice)
        val content: TextView = itemView.findViewById(R.id.content_text)
    }

    private fun bindHeader(holder: HeaderViewHolder, item: RecyclerItem) {

        if(item.isOpened) {
            holder.arrow.setImageResource(R.drawable.ic_arrow_close)
        } else {
            holder.arrow.setImageResource(R.drawable.ic_arrow_open)
        }

        holder.title.text = item.title
        holder.container.setOnClickListener {
            it.isClickable = false
            if(item.isOpened) {
                shrinkContents(holder.adapterPosition + 1)
            } else {
                expandContents(holder.adapterPosition + 1)
            }
            rotateArrow(holder, item, it)
        }
    }

    private fun bindContent(holder: ContentViewHolder, item: RecyclerItem, position: Int) {
        resizeContent(holder, item.isOpened)
        var count = item.content!!.count;
        holder.content.text = item.content!!.item_name
        holder.textViewCount.text=item.content.count.toString()
        holder.textViewPrice.text=item.content.item_amount.toString()
        holder.buttonAdd.setOnClickListener {
            count++
            item.content.count=count
            items.add(item)
            notifyDataSetChanged()
        }
        holder.buttonRemove.setOnClickListener{
            if (item.content.count>0){
                count--
                item.content.count=count
                items.remove(item)
                notifyDataSetChanged()
            }
        }
    }


    /**
     * this is the expandable trick.
     * resize each item to width 0, height 0, then
     * notifyItemRangeChanged do collapse / expand beautifully with their basic animation.
     */
    private fun resizeContent(holder: ContentViewHolder, isOpened: Boolean) {
        val container = holder.backgroundContainer
        if(isOpened) {
            container.visibility = View.VISIBLE
            container.layoutParams = FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        } else {
            container.visibility = View.GONE
            container.layoutParams = FrameLayout.LayoutParams(0, 0)
        }
    }

    private fun rotateArrow(holder: HeaderViewHolder, item: RecyclerItem, container: View) {
        val fromRotation = 0f
        val toRotation = 180f
        val rotateAnim = RotateAnimation(
            fromRotation, toRotation,
            RotateAnimation.RELATIVE_TO_SELF, 0.5f,
            RotateAnimation.RELATIVE_TO_SELF, 0.5f
        )

        rotateAnim.duration = 300
        rotateAnim.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation) {}

            override fun onAnimationEnd(animation: Animation) {
                if (item.isOpened) {
                    holder.arrow.clearAnimation()
                    holder.arrow.setImageResource(R.drawable.ic_arrow_open)
                    item.isOpened = false
                } else {
                    holder.arrow.clearAnimation()
                    holder.arrow.setImageResource(R.drawable.ic_arrow_close)
                    item.isOpened = true
                }
                container.isClickable = true
            }

            override fun onAnimationRepeat(animation: Animation) {}
        })
        rotateAnim.fillAfter = true // if false, the animation will reset
        holder.arrow.startAnimation(rotateAnim)
    }

    private fun expandContents(startPosition: Int) {
        var endPosition = startPosition
        while (itemList.size > endPosition && itemList[endPosition].type == CONTENT) {
            itemList[endPosition].isOpened = true
            endPosition++
        }

        notifyItemRangeChanged(startPosition, endPosition - 1)
    }

    private fun shrinkContents(startPosition: Int) {
        var endPosition = startPosition
        while (itemList.size > endPosition && itemList[endPosition].type == CONTENT) {
            itemList[endPosition].isOpened = false
            endPosition++
        }
        notifyItemRangeChanged(startPosition, endPosition - 1)
    }
}