package com.akshay.wibmo.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.akshay.wibmo.ui.home.HomeRepository
import com.akshay.wibmo.ui.home.HomeViewModel

class ViewModelFactory (private val homeRepository: HomeRepository) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return HomeViewModel(homeRepository) as T
    }

}