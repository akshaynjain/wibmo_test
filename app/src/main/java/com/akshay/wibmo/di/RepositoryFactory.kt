package com.akshay.wibmo.di

import com.akshay.wibmo.network.Api
import com.akshay.wibmo.network.Client
import com.akshay.wibmo.ui.home.HomeRepository

object RepositoryFactory {
    fun createHomeRepository() : HomeRepository {
        val githubApi = Client.instance.retrofit.create(Api::class.java)
        return HomeRepository(githubApi)
    }
}