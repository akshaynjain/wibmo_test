package com.akshay.wibmo.ui.home

import android.content.Context
import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.akshay.wibmo.R
import com.akshay.wibmo.model.BannersList
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.BitmapImageViewTarget
import java.lang.Exception


class BannerAdapter(var context: Context, val imageUrls:ArrayList<BannersList>) : PagerAdapter() {

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val inflater = LayoutInflater.from(context)
        val viewItem: View = inflater.inflate(R.layout.layout_banner, container, false)
        val imageView = viewItem.findViewById<View>(R.id.imageView) as ImageView
        var url:String=imageUrls[position].imageUrl.replace("http","https")
        try {
            Glide.with(context).asBitmap().load(url).into(
                BitmapImageViewTarget(imageView))
        }catch (e:Exception){
            e.printStackTrace()
        }


        (container as ViewPager).addView(viewItem)
        return viewItem
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view==`object`
    }

    override fun getCount(): Int {
        return imageUrls.size
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        (container as ViewPager).removeView(`object` as View)
    }
}