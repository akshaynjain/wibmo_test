package com.akshay.wibmo.ui.kart

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.akshay.wibmo.R
import com.akshay.wibmo.adapter.KartAdapter
import com.akshay.wibmo.model.RecyclerItem
import kotlinx.android.synthetic.main.kart_fragment.*


class KartFragment : Fragment() {

    lateinit var navController: NavController
    private lateinit var adapter: KartAdapter

    companion object {
        fun newInstance() = KartFragment()

    }

    private lateinit var viewModel: KartViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.kart_fragment, container, false)


        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        var items:ArrayList<RecyclerItem> = requireArguments().getParcelableArrayList<RecyclerItem>("items") as ArrayList<RecyclerItem>
        println(items)
        recyclerView.layoutManager = LinearLayoutManager(context)
        adapter = KartAdapter(requireContext())
        recyclerView.adapter = adapter
        adapter.setData(items)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(KartViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireActivity().onBackPressedDispatcher.addCallback(this) {
                navController.navigate(R.id.nav_home)
        }
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()){
            android.R.id.home -> {

                navController.navigate(R.id.nav_home)

            }
        }
        return super.onOptionsItemSelected(item)
    }



}