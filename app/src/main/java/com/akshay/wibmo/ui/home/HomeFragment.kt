package com.akshay.wibmo.ui.home

import android.graphics.Canvas
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.addCallback
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.ItemTouchUIUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.akshay.wibmo.R
import com.akshay.wibmo.adapter.ExpandableAdapter
import com.akshay.wibmo.di.RepositoryFactory
import com.akshay.wibmo.di.ViewModelFactory
import com.akshay.wibmo.model.BannersList
import com.akshay.wibmo.model.Products
import com.akshay.wibmo.model.RecyclerItem
import com.hyunstyle.exrecyclerview.util.SwipeItemTouchHelper
import com.hyunstyle.exrecyclerview.util.SwipeItemTouchListener
import kotlinx.android.synthetic.main.fragment_home.*
import java.util.*
import kotlin.collections.ArrayList


class HomeFragment : Fragment(), SwipeItemTouchListener {

    private lateinit var homeViewModel: HomeViewModel
    private  lateinit var bannersList:ArrayList<BannersList>
    private  lateinit var products:Products
    private lateinit var adapter: ExpandableAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        homeViewModel = ViewModelProvider(this,ViewModelFactory(RepositoryFactory.createHomeRepository())).get(HomeViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_home, container, false)
        homeViewModel.text.observe(viewLifecycleOwner, Observer {

        })

        // Swipe
        val swipeHelper = SwipeItemTouchHelper(0, ItemTouchHelper.LEFT, this)
        ItemTouchHelper(swipeHelper).attachToRecyclerView(recyclerView)

        homeViewModel.bannersList.observe(viewLifecycleOwner, Observer {
            println(it.toString())
            bannersList=it

            activity?.runOnUiThread {
                viewPager.adapter=BannerAdapter(requireContext(),bannersList)
                tabs.setupWithViewPager(viewPager,true)
            }

        })
        homeViewModel.productsList.observe(viewLifecycleOwner, Observer {
            println(it.toString())
            products=it
            adapter.setData(getData())
        })
        homeViewModel.getBanners()
        homeViewModel.getProducts()

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        recyclerView.layoutManager = LinearLayoutManager(context)
        // ExpandableAdapter
        adapter = ExpandableAdapter(requireContext())
        recyclerView.adapter = adapter
        button.setOnClickListener {
            try {
                var items = adapter.getData()
                if (!items.isEmpty()) {
                    for (i in items) {
                        println(i.content.toString())
                        println(i.title)
                    }
                    val bundle = bundleOf("items" to items)
                    Navigation.findNavController(view).navigate(R.id.kartFragment, bundle)
                }else{
                  val toast = Toast.makeText(requireContext(),"Please add few items",Toast.LENGTH_LONG)
                    toast.show()
                }
            }catch (e:Exception){
                e.printStackTrace()
            }
        }

        try {
            val timerObj = Timer()
            val timerTaskObj: TimerTask = object : TimerTask() {
                override fun run() {
                    activity?.runOnUiThread {
                        if (viewPager!=null)
                            viewPager.currentItem=((viewPager.currentItem+1)% 4)
                    }
                }
            }
            timerObj.schedule(timerTaskObj, 0, 15000)
        }catch (e:Exception){
            e.printStackTrace()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // This callback will only be called when MyFragment is at least Started.
        val callback = requireActivity().onBackPressedDispatcher.addCallback(this) {
            handleOnBackPressed()
        }

        // The callback can be enabled or disabled here or in the lambda
    }


    /**
     * get data example.
     * actually, this function should be modified to do some network requests like Retrofit 2.0.
     * after that, add Items to List using builder pattern.
     */
    private fun getData() : ArrayList<RecyclerItem> {
        val ret = ArrayList<RecyclerItem>()

        for (i in products.categories) {
            val header = RecyclerItem.Builder()
                .type(ExpandableAdapter.HEADER)
                .title(i.category_name)
                .build()

            ret.add(header)

            for(j in i.category_items) {
                val content = RecyclerItem.Builder()
                    .type(ExpandableAdapter.CONTENT)
                    .content(j)
                    .build()

                ret.add(content)
            }
        }
        return ret
    }

    // SwipeItemTouchListener override area

    override fun onSwiped(holder: RecyclerView.ViewHolder, direction: Int) {

        if(holder is ExpandableAdapter.ContentViewHolder) {
            val swipedIndex = holder.adapterPosition

        }
    }

    override fun onSelectedChanged(holder: RecyclerView.ViewHolder?, actionState: Int, uiUtil: ItemTouchUIUtil) {
        when(actionState) {
            ItemTouchHelper.ACTION_STATE_SWIPE -> {
                if(holder is ExpandableAdapter.ContentViewHolder) {
                    uiUtil.onSelected(holder.container)
                }
            }
        }
    }

    override fun onChildDraw(
        c: Canvas,
        recyclerView: RecyclerView,
        holder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean,
        uiUtil: ItemTouchUIUtil
    ) {
        when(actionState) {
            ItemTouchHelper.ACTION_STATE_SWIPE -> {
                if(holder is ExpandableAdapter.ContentViewHolder) {
                    uiUtil.onDraw(c, recyclerView, holder.container, dX, dY, actionState, isCurrentlyActive)
                }
            }
        }
    }

    override fun onChildDrawOver(
        c: Canvas,
        recyclerView: RecyclerView,
        holder: RecyclerView.ViewHolder?,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean,
        uiUtil: ItemTouchUIUtil
    ) {
        when(actionState) {
            ItemTouchHelper.ACTION_STATE_SWIPE -> {
                if(holder is ExpandableAdapter.ContentViewHolder) {
                    uiUtil.onDrawOver(c, recyclerView, holder.container, dX, dY, actionState, isCurrentlyActive)
                }
            }
        }
    }

    override fun clearView(recyclerView: RecyclerView, holder: RecyclerView.ViewHolder, uiUtil: ItemTouchUIUtil) {
        if(holder is ExpandableAdapter.ContentViewHolder) {
            uiUtil.clearView(holder.container)
        }
    }


}