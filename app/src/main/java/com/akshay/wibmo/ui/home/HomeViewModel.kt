package com.akshay.wibmo.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.akshay.wibmo.model.BannersList
import com.akshay.wibmo.model.Categories
import com.akshay.wibmo.model.Products

class HomeViewModel(val homeRepository: HomeRepository) : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is home Fragment"
    }
    val text: LiveData<String> = _text

    private val _bannersList : MutableLiveData<ArrayList<BannersList>> = MutableLiveData()
    val bannersList : LiveData<ArrayList<BannersList>> = _bannersList

    private val _productsList : MutableLiveData<Products> = MutableLiveData()
    val productsList : LiveData<Products> = _productsList

    fun getBanners(){
        homeRepository.fetchBanners()
            .subscribe{
                _bannersList.postValue(it)
            }
    }

    fun getProducts(){
        homeRepository.fetchProducts()
            .subscribe{
                _productsList.postValue(it)
            }
    }
}