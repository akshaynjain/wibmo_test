package com.akshay.wibmo.ui.home

import com.akshay.wibmo.model.BannersList
import com.akshay.wibmo.model.Products
import com.akshay.wibmo.network.Api
import kotlin.collections.ArrayList
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class HomeRepository(val api: Api) {
    fun fetchBanners(): Observable<ArrayList<BannersList>>{
        return Observable.create{emitter->
            api.getBanners()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.body() != null) {
                        emitter.onNext(it.body()!!)
                    }
                }, {
                    it.printStackTrace()
                })
        }
    }

    fun fetchProducts():Observable<Products>{
        return Observable.create { emitter ->
            api.getProducts()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.body() != null) {
                        emitter.onNext(it.body()!!)
                    }
                }, {
                    it.printStackTrace()
                })
        }
    }
}