package com.akshay.wibmo.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BannersList(@SerializedName("name") var name : String = "",
                       @SerializedName("imageUrl") var imageUrl : String = "",
                       @SerializedName("imageUrlWebp") var imageUrlWebp : String = "") : Parcelable

@Parcelize
data class Items(@SerializedName("item_id") var item_id : Long = 0,
                          @SerializedName("item_name") var item_name : String = "",
                          @SerializedName("item_amount") var item_amount : Double = 0.0,
                            var count: Int =0) : Parcelable

@Parcelize
data class Categories(@SerializedName("category_id")var  category_id:Long =0,
                         @SerializedName("category_name")var category_name:String="",
                         @SerializedName("category_items")var category_items:ArrayList<Items>) : Parcelable

@Parcelize
data class Products(var categories: ArrayList<Categories>) : Parcelable
