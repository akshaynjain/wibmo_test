package com.akshay.wibmo.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * RecyclerItem object using builder pattern.
 */

@Parcelize
class RecyclerItem(internal val type: Int,
                   internal val title: String?,
                   internal val content: Items?,
                   internal var isOpened: Boolean) : Parcelable {

    data class Builder(
        private var type: Int = 0,
        private var title: String? = null,

        private var content: Items? = null,
        private var isOpened: Boolean = false) {

        fun type(type: Int) = apply { this.type = type }
        fun title(title: String) = apply { this.title = title }

        fun content(content: Items) = apply { this.content = content }
        fun isOpened(isOpened: Boolean) = apply { this.isOpened = isOpened }
        fun build() = RecyclerItem(
            type,
            title,
            content,
            isOpened
        )
    }

}