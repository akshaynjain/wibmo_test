package com.akshay.wibmo.network

import com.akshay.wibmo.model.BannersList
import com.akshay.wibmo.model.Categories
import com.akshay.wibmo.model.Products
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface Api{
    @GET("/Payzapp/TEMP_Testing/interview/json/banners.json")
    fun getBanners(): Single<Response<ArrayList<BannersList>>>

    @GET("/Payzapp/TEMP_Testing/interview/json/ProductList.json")
    fun getProducts(): Single<Response<Products>>
}