package com.akshay.wibmo.network

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class Client private constructor(){
    private val API_BASE_URL = "https://in-mum-payzapp.s3.ap-south-1.amazonaws.com/"
    val retrofit: Retrofit

    init {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val httpClient = OkHttpClient.Builder().addInterceptor(interceptor).build()

        val builder = Retrofit.Builder()
            .baseUrl(API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())

        retrofit = builder.client(httpClient).build()
    }

    companion object {
        private var self: Client? = null

        val instance: Client
            get() {
                if (self == null) {
                    synchronized(Client::class.java) {
                        if (self == null) {
                            self =
                                Client()
                        }
                    }
                }
                return self!!
            }
    }
}